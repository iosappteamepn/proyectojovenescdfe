//
//  ListaDeYoutubeViewController.swift
//  proyectoJovenesCdfe
//
//  Created by DAVID MONCAYO on 18/8/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class ListaDeYoutubeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var SermonsYoutubeTableView: UITableView!
    let generarSermones = GenerarSermones()
    var sermonArray = [Sermon]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sermonArray = generarSermones.crearSermones()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sermonArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sermonCell") as! SermonTableViewCell
        cell.sermon = self.sermonArray[indexPath.row]
        cell.fillData()
        return cell
    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
