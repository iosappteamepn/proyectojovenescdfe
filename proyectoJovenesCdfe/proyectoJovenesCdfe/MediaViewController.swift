//
//  MediaViewController.swift
//  proyectoJovenesCdfe
//
//  Created by alexfb on 28/7/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class MediaViewController: UIViewController, ScrollPagerDelegate,UITableViewDelegate, UITableViewDataSource {
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCell")!
        return cell as! UITableViewCell
    }
    
    
    @IBOutlet weak var scrollPager: ScrollPager!
    let facebookView = UITableView()
    let YoutuBeVid = UIWebView()
    @IBOutlet weak var scrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        loadYoutube(videoID: "KRUrF1DRqpE")
        
        
        let firstView = UILabel()
        firstView.backgroundColor = UIColor.white
        firstView.text = "first View"
        firstView.textAlignment = .center
        
        let secondView = UILabel()
        secondView.backgroundColor = UIColor.white
        secondView.text = "second view"
        secondView.textAlignment = .center
        
        
        
        
        
        createFacebookView()
        
        let view1 = facebookView
        scrollPager.delegate = self
        scrollPager.addSegmentsWithTitlesAndViews(segments: [
            ("Facebook", firstView),
            ("Sermons", YoutuBeVid),
            ])
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createFacebookView(){
        facebookView.delegate = self
        facebookView.dataSource = self
        facebookView.tag = 1
        facebookView.frame.size = scrollView.frame.size
        facebookView.backgroundColor = UIColor.clear
        facebookView.tableFooterView = UIView()
        
    }
    
    func loadYoutube(videoID:String) {
        guard
            let youtubeURL = URL(string: "https://www.youtube.com/embed/\(videoID)")
            else { return }
        YoutuBeVid.loadRequest( URLRequest(url: youtubeURL) )
    }
    
    
    func scrollPager(scrollPager: ScrollPager, changedIndex: Int) {
        print("scrollPager index changed: \(changedIndex)")
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


