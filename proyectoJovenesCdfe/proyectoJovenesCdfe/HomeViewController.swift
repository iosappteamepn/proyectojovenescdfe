//
//  HomeViewController.swift
//  proyectoJovenesCdfe
//
//  Created by alexfb on 28/7/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, ScrollPagerDelegate {

    @IBOutlet weak var homeScrollPager: ScrollPager!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let bm=BackEndManager()
        bm.verRegistros2()
        NotificationCenter.default.addObserver(self, selector: #selector(actualizarDatos), name: NSNotification.Name("object2"), object: nil)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func actualizarDatos(_ notification:Notification){
        let json=notification.userInfo?["object2"] as! NSArray
        let dict=json[0] as! NSDictionary
        print("el texto es \(dict["text"]!)")
        
        let firstView = UILabel()
        firstView.backgroundColor = UIColor.cyan
        firstView.text = "Welcome to CDFEBible"
        firstView.textAlignment = .center
        
        let secondView = UILabel()
        secondView.backgroundColor = UIColor.white
        secondView.numberOfLines=4
        secondView.text = "\(dict["text"]!)"
        secondView.textAlignment = .center
        
        homeScrollPager.delegate = self
        homeScrollPager.addSegmentsWithTitlesAndViews(segments: [
            ("Welcome", firstView),
            ("Verse of the day", secondView)
            ])

        //self.htmlString="\(json["content"]!)"
        //htmlWebView.loadHTMLString(htmlString, baseURL: nil)
        
    }
    
}
