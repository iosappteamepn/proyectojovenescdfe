//
//  SermonTableViewCell.swift
//  proyectoJovenesCdfe
//
//  Created by DAVID MONCAYO on 18/8/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class SermonTableViewCell: UITableViewCell {
    var sermon:Sermon!
    @IBOutlet weak var sermonVideo: UIWebView!
    @IBOutlet weak var sermonNombre: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func fillData(){
        self.sermonNombre.text = self.sermon.nombre
        self.loadYoutube(videoID: self.sermon.id!)
    }
    
    func loadYoutube(videoID:String) {
        guard
            let youtubeURL = URL(string: "https://www.youtube.com/embed/\(videoID)")
            else { return }
        self.sermonVideo.loadRequest( URLRequest(url: youtubeURL) )
    }
}
