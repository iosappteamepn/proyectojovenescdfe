//
//  BackEndManager.swift
//  proyectoJovenesCdfe
//
//  Created by user1 on 18/8/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import Foundation
import Alamofire

class BackEndManager {
    func verRegistros(book:String,chapter:String){
    let urlString="https://nodejs.bible.com/api/bible/chapter/3.1?id=1&reference=\(book).\(chapter)"
    Alamofire.request(urlString).responseJSON { response in
        debugPrint(response)
        
        if let json = response.result.value {
            
            
            NotificationCenter.default.post(name: NSNotification.Name("object"), object: nil, userInfo: ["object" : json])
            
        }else{
            print("error")
            
        }
        
    }
    
}
    
    func verRegistros2(){
        let urlString="http://labs.bible.org/api/?passage=votd&type=json"
        Alamofire.request(urlString).responseJSON { response in
            debugPrint(response)
            
            if let json = response.result.value {
                
                
                NotificationCenter.default.post(name: NSNotification.Name("object2"), object: nil, userInfo: ["object2" : json])
                
            }else{
                print("error")
                
            }
            
        }
        
    }
    
}
