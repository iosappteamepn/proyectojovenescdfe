//
//  ContentViewController.swift
//  proyectoJovenesCdfe
//
//  Created by user1 on 18/8/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit


class ContentViewController: UIViewController {

    @IBOutlet weak var htmlWebView: UIWebView!
    
    var content=[String]()
    var htmlString:String=""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let bm=BackEndManager()
        bm.verRegistros(book: content[1], chapter: content[0])
        NotificationCenter.default.addObserver(self, selector: #selector(actualizarDatos), name: NSNotification.Name("object"), object: nil)
        
        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("la lista seleccionada es: \(content[0]) y el otro valor es: \(content[1])")
        
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func actualizarDatos(_ notification:Notification){
        let json=notification.userInfo?["object"] as! NSDictionary
        
        self.htmlString="\(json["content"]!)"
        htmlWebView.loadHTMLString(htmlString, baseURL: nil)
        
    }

}
