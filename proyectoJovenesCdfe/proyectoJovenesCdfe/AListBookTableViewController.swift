//
//  AListBookTableViewController.swift
//  proyectoJovenesCdfe
//
//  Created by user1 on 18/8/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class AListBookTableViewController: UITableViewController {
    
    @IBOutlet var booksTableView: UITableView!
    var books=[["Genesis","GEN"],["Exodus","EXO"],["Leviticus","LEV"],["Numbers","NUM"],["Deuteronomy","DEU"],["Joshua","JOS"],["Judges","JUD"],["Ruth","RUT"],["1 Samuel","1SA"],["2 Samuel","2SA"],["1 Kings","1KI"],["2 Kings","2KI"],["1 Chronicles","1CH"],["2 Chronicles","2CH"],["Ezra","EZR"],["Nehemiah","NEH"],["Esther","EST"],["Job","JOB"],["Psalms","PSA"],["Proverbs","PRO"],["Ecclesiastes","ECC"],["Song of Solomon","SON"],["Isaiah","ISA"],["Jeremiah","JER"],["Lamentations","LAM"],["Ezekiel","EZE"],["Daniel","DAN"],["Hosea","HOS"],["Joel","JOE"],["Amos","AMO"],["Obadiah","OBA"],["Jonah","JON"],["Micah","MIC"],["Nahum","NAH"],["Habakkuk","HAB"],["Zephaniah","ZEP"],["Haggai","HAG"],["Zechariah","ZEC"],["Malachi","MAL"],["Matthew","MAT"],["Mark","MAR"],["Luke","LUK"],["John","JOH"],["Acts","ACT"],["Romans","ROM"],["1 Corinthians","1CO"],["2 Corinthians","2CO"],["Galatians","GAL"],["Ephesians","EPH"],["Philippians","PHI"],["Colossians","COL"],["1 Thessalonians","1TH"],["2 Thessalonians","2TH"],["1 Timothy","1TI"],["2 Timothy","2TI"],["Titus","TIT"],["Philemon","PHI"],["Hebrews","HEB"],["James","JAM"],["1 Peter","1PE"],["2 Peter","2PE"],["1 John","1JO"],["2 John","2JO"],["3 John","3JO"],["Jude","JUD"],["Revelation","REV"]]
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return books.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "bookCell", for: indexPath)

        // Configure the cell...
        cell.textLabel?.text=books[indexPath.row][0]

        return cell
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier=="booksToChapter"{
            let destination=segue.destination as! AListChapterTableViewController

            
            //forma correcta
            destination.message=books[(booksTableView.indexPathForSelectedRow?.row)!][1]
        }
    }
 

}
