//
//  PlansViewController.swift
//  proyectoJovenesCdfe
//
//  Created by alexfb on 28/7/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class PlansViewController: UIViewController  {


    @IBOutlet weak var plansWebView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //let url=NSURL(string:"https://www.bible.com/es-ES/reading-plans");
        
        //let request=NSURLRequest(URL: url as! URL);
        
       // plansWebView.loadRequest(request);
        plansWebView.loadRequest(URLRequest(url: URL(string: "https://www.bible.com/es-ES/reading-plans")!))
        // Do any additional setup after loading the view.
        /*let url=URL(string: "https://www.bible.com/es-ES/reading-plans")
        if let unwrappedURL=url{
            let request=URLRequest(url: unwrappedURL)
            let session=URLSession.shared
            let task=session.dataTask(with: request){
                (data,response,error)in
                if error==nil{
                    self.plansWebView.loadRequest(request)
                }else{
                    print("Error: \(error)")
                }
            }
            task.resume()
        }*/
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
  
    
    

}
